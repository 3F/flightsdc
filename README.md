feature rich & light DC++ client for Direct Connect protocol (p2p)
![planning architecture](https://bitbucket.org/3F/flightsdc/downloads/soa.png)

### Основание проекта ###
Создание экосистемы для публикации данных, с привлечением расширенной функциональности в поиске и идентификации контента.

### [Читать подробности проекта](https://bitbucket.org/3F/flightsdc/wiki) ###